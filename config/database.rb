Sequel::Model.raise_on_save_failure = false # Do not throw exceptions on failure
Sequel::Model.plugin :json_serializer
Sequel::Model.db = case Padrino.env
  when :development then Sequel.connect("postgres://localhost/test", :loggers => [logger], :user => 'chuliang')
  when :production  then Sequel.connect("postgres://localhost/game_production",  :loggers => [logger])
  when :test        then Sequel.connect("postgres://localhost/game_test",        :loggers => [logger])
end
Sequel::Model.db.extension(:pagination)
