class Category < Sequel::Model
  plugin :validation_helpers
  def validate
    super
    validates_presence [:course_name]
  end
end
