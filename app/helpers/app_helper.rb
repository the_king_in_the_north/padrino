class AppHelper
  def self.pagination(datas)
    {
      data: datas,
      page_size: datas.page_size,
      page_count: datas.page_count,
      current_page: datas.current_page,
      next_page: datas.next_page,
      prev_page: datas.prev_page
    }.to_json
  end
end

  