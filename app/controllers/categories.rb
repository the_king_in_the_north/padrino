Game::App.controllers :categories do
  
  # get :index, :map => '/foo/bar' do
  #   session[:foo] = 'bar'
  #   render 'index'
  # end

  # get :sample, :map => '/sample/url', :provides => [:any, :js] do
  #   case content_type
  #     when :js then ...
  #     else ...
  # end

  # get :foo, :with => :id do
  #   "Maps to url '/foo/#{params[:id]}'"
  # end

  # get '/example' do
  #   'Hello world!'
  # end

  get :index, :map => '/categoires', :with => [:page, :per] do
    @categoires = Category.order(:id).paginate(params[:page].to_i, params[:per].to_i)
    AppHelper.pagination(@categoires)
  end

  get :show, :map => '/categoires/:id' do
    Category[params[:id]].to_json
  end

 
  
end
